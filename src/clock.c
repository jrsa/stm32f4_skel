#include <stm32f405xx.h>

void setup_sysclk_to_mco2() {
    // MCO2, SYSCLK
    // it's important to set this up before starting to use the PLL,
    // unfortunately i dont understand why
    RCC->AHB1ENR |= RCC_AHB1ENR_GPIOCEN;

    GPIOC->MODER &= ~(GPIO_MODER_MODE9_Msk);
    GPIOC->MODER |= GPIO_MODER_MODE9_1;

    GPIOC->AFR[0] |= (uint8_t)1;
    GPIOC->OSPEEDR |= GPIO_OSPEEDR_OSPEED9_Msk;

    // for MCO2, output sysclk
    RCC->CFGR &= (RCC_CFGR_MCO2_Msk);

    RCC->CFGR &= ~RCC_CFGR_MCO2PRE_Msk;
    RCC->CFGR |= (0b110 << RCC_CFGR_MCO2PRE_Pos);
}

void setup_pll_sysclk() {
    //-------------------------------------------------------------------------
    // going to start with setting up high speed osc input, in bypass mode,
    // since the nucleo board im using provides the clock input from another
    // mcu. if you only have one mcu with a crystal osc like most setups, you
    // would leave the HSEBYP bit low to use the osc starter circuit (?)
    RCC->CR |= RCC_CR_HSEON | RCC_CR_HSEBYP;

    // wait for HSE signal to stabilize
    while(!(RCC->CR & RCC_CR_HSERDY))
        ;

    // turn pll off (we are going to reconfigure it)
    RCC->CR &= (uint32_t)(~RCC_CR_PLLON);

    // wait for PLL to shut off
    while((RCC->CR & RCC_CR_PLLRDY) != 0)
        ;

    // need to divide 8MHz HSE input by 4 to get a 2MHz input to PLL
    // (this is the PLLM parameter)
    RCC->PLLCFGR &= ~(RCC_PLLCFGR_PLLM_Msk);
    RCC->PLLCFGR |= (4 & RCC_PLLCFGR_PLLM_Msk);

    // use HSE clock as input to PLL
    RCC->PLLCFGR |= RCC_PLLCFGR_PLLSRC_HSE;

    // crank PLL VCO output to 336MHz! (HSE input, 8MHz, divided by 4 (M stage)
    // to get 2MHz, multiplied by 168 to get 336 (N stage) and finally divided
    // again by 2 (P stage) to get back to 168MHz
    RCC->PLLCFGR &= ~(RCC_PLLCFGR_PLLN_Msk);
    RCC->PLLCFGR |= (168 << RCC_PLLCFGR_PLLN_Pos);

    // zero out `P` (PLL output divider) for a division of 2 (yielding 168MHz)
    RCC->PLLCFGR &= ~(RCC_PLLCFGR_PLLP_Msk);

    RCC->PLLCFGR |= (7 << 24);

    RCC->CFGR &= ~(RCC_CFGR_PPRE1_Msk | RCC_CFGR_PPRE2_Msk | RCC_CFGR_HPRE_Msk); // clear APB1/2 div
    RCC->CFGR |= RCC_CFGR_HPRE_DIV1;                
    RCC->CFGR |= RCC_CFGR_PPRE1_DIV4;                
    RCC->CFGR |= RCC_CFGR_PPRE2_DIV2;                

    RCC->CR |= RCC_CR_PLLON; // im scared
    
    while(!(RCC->CR & RCC_CR_PLLRDY))
        ;

    RCC->CFGR &= (uint32_t)((uint32_t)~(RCC_CFGR_SW));
    RCC->CFGR |= RCC_CFGR_SW_PLL;

    while ((RCC->CFGR & (uint32_t)RCC_CFGR_SWS ) != RCC_CFGR_SWS_PLL);
}
