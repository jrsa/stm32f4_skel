#include "stm32f405xx.h"
#include "clock.h" // clock tree, pll etc

volatile int count = 0;

int main(int argc, const char *argv[])
{
    //-------------------------------------------------------------------------
    // copied from vendor startup code

    // coprocessor (FPU) access
    SCB->CPACR |= ((3UL << 10*2)|(3UL << 11*2));  /* set CP10 and CP11 Full Access */

    // onboard ldo scaling?
    RCC->APB1ENR |= RCC_APB1ENR_PWREN;
    PWR->CR |= PWR_CR_VOS;

    // enable I-Cache, D-Cache, and set 5 wait states for flash (to compensate
    // for core clock being too fast for the flash
    FLASH->ACR = FLASH_ACR_ICEN |FLASH_ACR_DCEN |FLASH_ACR_LATENCY_5WS;
    //-------------------------------------------------------------------------

    // currently using for PA5, LED, and PA8, MCO1
    RCC->AHB1ENR |= RCC_AHB1ENR_GPIOAEN;

    // setup PA5 as a normal output pin for the green LED
    GPIOA->MODER |= (1 << (5 << 1));
    GPIOA->OSPEEDR |= (3 << (5 << 1));


#ifdef SYSCLK_MCO2
    setup_sysclk_to_mco2();
#endif
    
    setup_pll_sysclk();

    int encoder_isr_mask = (1 << 6); // | (1 << 7);
    EXTI->IMR |= encoder_isr_mask;

    EXTI->RTSR |= encoder_isr_mask; 

    NVIC_EnableIRQ(EXTI9_5_IRQn);
    NVIC_SetPriority(EXTI9_5_IRQn, 1);
    
    while (1) {
    }
}

void EXTI9_5_IRQHandler() {

    if (EXTI->PR & (1 << 6)) {
        if (GPIOA->IDR & (1 << 7)) {
            GPIOA->ODR ^= (1 << 5);
            count++;
        }
        else {
            count--;
        }

        // clear interrupt flag
        EXTI->PR |= (1 << 6);
    }
}
