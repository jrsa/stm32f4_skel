TARGET = play

ROOT ?= .
TARGET_DIR ?= .

# XXX do a glob
C_SOURCES = src/main.c \
	    src/clock.c

C_INCLUDES = -Iinclude
C_INCLUDES += -Ivendor/arm/include
C_INCLUDES += -Ivendor/st/include

ASM_SOURCES += vendor/st/src/startup_stm32f405xx.s
LDSCRIPT = vendor/st/stm32f407.ld

BUILD_DIR = $(ROOT)/build/$(TARGET)/
TARGET_BIN = $(BUILD_DIR)/$(TARGET).bin

OPT = -O0

# path to gcc
BINPATH = /usr/local/bin
PREFIX = arm-none-eabi-
CC = $(BINPATH)/$(PREFIX)gcc
AS = $(BINPATH)/$(PREFIX)as
CP = $(BINPATH)/$(PREFIX)objcopy
SZ = $(BINPATH)/$(PREFIX)size
HEX = $(CP) -O ihex
BIN = $(CP) -O binary -S
DB = $(BINPATH)/$(PREFIX)gdb

# XXX this part is a mess
MCU = -mcpu=cortex-m4 -mthumb -mlittle-endian  -mfloat-abi=hard -mfpu=fpv4-sp-d16

ASFLAGS = $(MCU) $(AS_DEFS) $(AS_INCLUDES) $(OPT) -Wall -fdata-sections
CFLAGS = $(MCU) -ffunction-sections $(C_INCLUDES) -DARM_MATH_CM4 -g3 $(OPT)

LIBS = -lc -lm -lnosys 
LDFLAGS = $(MCU) -specs=nano.specs -T$(LDSCRIPT) $(LIBS) -Wl,-Map=$(BUILD_DIR)/$(TARGET).map,--cref -Wl,--gc-sections

all: $(BUILD_DIR)/$(TARGET).elf $(BUILD_DIR)/$(TARGET).hex $(BUILD_DIR)/$(TARGET).bin 

# any c/cpp/embedded project will have this

OBJECTS = $(addprefix $(BUILD_DIR)/,$(notdir $(C_SOURCES:.c=.o)))
vpath %.c $(sort $(dir $(C_SOURCES)))
OBJECTS += $(addprefix $(BUILD_DIR)/,$(notdir $(ASM_SOURCES:.s=.o)))
vpath %.s $(sort $(dir $(ASM_SOURCES))) 

$(BUILD_DIR)/%.o: %.c | $(BUILD_DIR) 
	@$(CC) -c $(CFLAGS) -Wa,-a,-ad,-alms=$(BUILD_DIR)/$(notdir $(<:.c=.lst)) $< -o $@
	@echo "[CC]\t $<"

$(BUILD_DIR)/%.o: %.s | $(BUILD_DIR)
	@$(AS) -c $(AFLAGS) $< -o $@
	@echo "[AS]\t $<"

$(BUILD_DIR)/$(TARGET).elf: $(OBJECTS) Makefile
	@$(CC) $(OBJECTS) $(LDFLAGS) -o $@
	@echo "[LD]\t $^: $@\n"
	@$(SZ) $@

$(BUILD_DIR)/%.hex: $(BUILD_DIR)/%.elf | $(BUILD_DIR)
	@$(HEX) $< $@
	@echo "[OBJCOPY]\t$< -> $@"
	
$(BUILD_DIR)/%.bin: $(BUILD_DIR)/%.elf | $(BUILD_DIR)
	@$(BIN) $< $@	
	@echo "[OBJCOPY]\t$< -> $@"

$(BUILD_DIR):
	mkdir -p $@	

.phony: clean emu emugdb

# couldnt figure out how to do these in one recipe
emu:
	qemu-system-arm -s -S -nographic -M netduinoplus2

# dont forget to do a "monitor system_reset" before you "load"
emugdb: $(BUILD_DIR)/$(TARGET).elf
	arm-none-eabi-gdb $(BUILD_DIR)/$(TARGET).elf -iex "tar ext :1234"


clean:
	@rm -r $(BUILD_DIR)/$(TARGET).elf
	@rm -r $(BUILD_DIR)/$(TARGET).hex
	@rm -r $(BUILD_DIR)/$(TARGET).bin
	@rm -r $(BUILD_DIR)/*.o
